# Learning Java
June 9, 2023

Course made by Chand Sheikh, an Indian developer currently residing an Ireland.  He has two Masters degrees.  One in AI, and one in Computer Applications. He has 8 years of teaching experience, worked for Microsoft, supported and developed Windows 8.

[Course Link](https://www.udemy.com/course/full-stack-java-developer-java/learn/lecture/24201616#overview)

## History

- Java 1.0 came out in 1995
- C-like notation
- General purpose usage, uniform and simple
- Unlike C, Java is true OOO programming language. 
- Write once, run anywhere. If we want to run an app on different operating systems we would have to compile the code on different operating systems. This can be tedious. Java introduced a layer between OS and App, called `Java Virtual Machine`, that sits between the app and the code. This means we have platform independent building.  

### Support

- IDE's 
- JAVAEE
- Spring
- Android studio
- JAVAFX
- Jax-rs

